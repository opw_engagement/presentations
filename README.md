# Presentations

Presentations that have been given on open practical work:

## 2019

1. Timothy D. Drysdale, "Remote Teaching Laboratories - Pedagogy, Performance, and Pooling", NI Academic User Forum, University of Birmingham, UK, Jan 18, 2019

2. Timothy D. Drysdale, Victoria Dishon, Andrew Weightman, Stephen Watts, Richard Lewis, "Remote laboratories - requirements capture for a national pooling infrastructure," HE Academy STEM Conference, Birmingham, UK, Jan 30-31, 2019

3. Timothy D. Drysdale "Learning digital literacies in a STEM context," HE Academy STEM Conference, Birmingham, UK, Jan 30-31, 2019

## License
The content (e.g. presentations) in this repository is licensed under the [Creative Commons Attribution-ShareAlike 4.0 International License](http://creativecommons.org/licenses/by-sa/4.0/) content,  while the openPracticalWork softare itself, and any snippets shown in the presentations, are licensed under the [GNU GPL-3.0 License](https://www.gnu.org/licenses/gpl-3.0.txt).




